<?php

return [
    'userblocked'             => 'Usuario bloqueado con éxito. No verás más publicaciones de este usuario.',
    'reportcommunity'             => 'Hemos recibido tu reporte, no verás más :type y un administrador revisará el caso.',
    'thecomment'             => 'este commentario',
    'thepost'             => 'esta publicación',
];
