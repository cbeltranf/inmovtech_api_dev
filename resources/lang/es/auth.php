<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'El usuario y contraseña ingresados no coinciden con nuestros registros.',
    'emailfailed'   => 'El correo ingresado no se encuentra asociado a ninguna compañia.',
    'logout'   => 'Se ha cerrado sesión con éxito.',
    'inactive'   => 'El usuario se encuentra inactivo.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'test'   => 'Esta es una prueba.',
    'nomatch'   => 'Hemos comprobado que los datos ingresados no coinciden con el registro. Por favor, ayúdanos a verificar tu identidad e inténtalo de nuevo.',
    'smssend'   => 'Hemos enviado un codigo de recuperación de contraseña a tu telefono celular.',
    'emailsend'   => 'Hemos enviado un codigo de recuperación de contraseña a tu correo electrónico.',
    'smssent'   => 'SMS enviado con éxito.',
    'emailsent'   => 'Email enviado con éxito.',
    'hi'   => 'Hola',
    'mailrecovery'   => 'Se ha registrado una petición para cambiar la contraseña de tu cuenta de My Digital Card.',
    'entercode'   => 'Ingresa el siguiente código en tu aplicación:',
    'onlysocial'   => 'Tu usuario solo permite iniciar sesión a través de tu cuenta corporativa de GMail.',
    'onlyuser'   => 'Tu usuario no permite el inicio de sesión mediante cuenta de GMail.',
    'noplatform'   => 'Información de plataforma de usuario no detectada.',
    'profileblocked'   => 'Tu perfil de usuario no tiene permitido el acceso al panel de administración.',
    'recoverycodeexpired'   => 'Codigo de recuperacion expirado, por favor genera uno nuevamente.',
];
