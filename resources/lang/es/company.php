<?php

return [

    'aliasused'   => 'El alias que ingresaste pertenece a una compañia que ya se encuetra registrada en nuestro sistema.',
    'freemail'   => 'My Digital Card no permite el uso de cuentas de correo gratuitas, por favor ingresa tu correo corporativo.',
    'companyregok'   => 'Tu cuenta se ha creado con éxito. Pronto recibirás un correo electrónico con los pasos necesarios para completar tu registro.',
    'companyemailok'   => 'Registro exitoso!. Para completar el registro de tu empresa en My Digital Card te invitamos a dar click en el siguiente enlace.',
    'socialregok'   => 'Red social creada con éxito.',
    'branchregok'   => 'Sucursal de la compañia creada con éxito.',
    'branchrupdok'   => 'Sucursal de la compañia actualizada con éxito.'
];
