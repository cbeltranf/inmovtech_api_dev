<?php

return [
    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => ':attribute debe ser una fecha posterior a :date.',
    'after_or_equal'       => ':attribute debe ser una fecha posterior o igual a :date.',
    'alpha'                => ':attribute sólo debe contener letras.',
    'alpha_dash'           => ':attribute sólo debe contener letras, números y guiones.',
    'alpha_num'            => ':attribute sólo debe contener letras y números.',
    'array'                => ':attribute debe ser un conjunto.',
    'before'               => ':attribute debe ser una fecha anterior a :date.',
    'before_or_equal'      => ':attribute debe ser una fecha anterior o igual a :date.',
    'emailtaken'      => 'El Email ingresado ya está asociado a otro usuario del sistema.',
    'massok'      => 'Carga masiva realizada con éxito.',
    'massemaildup'      => 'Los siguientes emails ya están siendo usados por otro usuario en el sistema o en este documento: ',
    'masspositions'      => 'Los siguientes cargos no se encuentran creados en el sistema: ',
    'massbranches'      => 'Las siguientes sucursales no se encuentran creadas en el sistema: ',
    'massnotify'      => 'Las opciones válidas para permisos de notificación son ("S" = Si, "N" = No), Las siguientes opciones no son válidas: ',
    'masssession'      => 'Las opciones válidas para tipo de inicio de sesión ("M" = User/Password, "S" = Gmail Login), Las siguientes opciones no son válidas: ',
    'massprofile'      => 'Los siguientes perfiles no se encuentran creados en el sistema: ',
    'masserrors'      => 'Soluciona los siguientes inconvenientes y vuelve a cargar el archivo: ',
    'freemail'   => 'My Digital Card no permite el uso de cuentas de correo gratuitas, por favor ingresa tu correo corporativo.',











];
