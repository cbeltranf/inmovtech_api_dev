<?php

return [
    'userblocked'             => 'User blocked successfully. You will not see any more posts from this user.',
    'reportcommunity'             => 'We have received your report, you will not see any more :type and an administrator will review the case.',
    'thecomment'             => 'this comment',
    'thepost'             => 'this post'
];
