<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'These credentials do not match our records.',
    'emailfailed'   => 'The mail entered is not associated with any company.',
    'inactive'   => 'The user is inactive.',
    'logout'   => 'Successfully logged out',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'test'   => 'This is a test.',
    'nomatch'   => 'We have verified that the data entered does not match the record. Please help us verify your identity and try again.',
    'smssend'   => 'We have sent a recovery code to your cell phone.',
    'emailsend'   => 'We have sent a recovery code to your email adress.',
    'smssent'   => 'SMS sent succesfully.',
    'emailsent'   => 'Email sent succesfully.',
    'hi'   => 'Hi',
    'mailrecovery'   => 'A request to change the password for your My Digital Card account has been registered.',
    'entercode'   => 'Enter the following recovery code in your application:',
    'onlysocial'   => 'Your user only allows you to log in through your GMail corporate account.',
    'onlyuser'   => 'Your user does not allow logging in via GMail account.',
    'noplatform'   => 'User platform information not detected.',
    'profileblocked'   => 'Your user profile is not allowed to access the admin panel.',
    'recoverycodeexpired'   => 'Expired recovery code, please generate one again.',


];
