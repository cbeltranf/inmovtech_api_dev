<?php

return [
    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => ':attribute debe ser una fecha posterior a :date.',
    'after_or_equal'       => ':attribute debe ser una fecha posterior o igual a :date.',
    'alpha'                => ':attribute sólo debe contener letras.',
    'alpha_dash'           => ':attribute sólo debe contener letras, números y guiones.',
    'alpha_num'            => ':attribute sólo debe contener letras y números.',
    'array'                => ':attribute debe ser un conjunto.',
    'before'               => ':attribute debe ser una fecha anterior a :date.',
    'before_or_equal'      => ':attribute debe ser una fecha anterior o igual a :date.',
    'emailtaken'      => 'The entered email is already associated with another user of the system.',
    'massok'      => 'Massive load made successfully.',
    'massemaildup'      => 'The following emails are already being used by another user in the system or in this document: ',
    'masspositions'      => 'The following charges are not created in the system: ',
    'massbranches'      => 'The following branches are not created in the system: ',
    'massnotify'      => 'Valid options for notification permissions are ("S" = Yes, "N" = No), The following options are not valid: ',
    'masssession'      => 'Valid options for type of login ("M" = User / Password, "S" = Gmail Login), The following options are not valid: ',
    'massprofile'      => 'The following profiles are not created in the system:',
    'masserrors'      => 'Solve the following issues and resend the file: ',
    'freemail'   => 'My Digital Card does not allow the use of free email accounts: ',



];
