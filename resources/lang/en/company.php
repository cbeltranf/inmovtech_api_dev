<?php

return [


    'aliasused'   => 'The alias you entered belongs to a company that is already registered in our system.',
    'freemail'   => 'My Digital Card does not allow the use of free email accounts, please enter your corporate email.',
    'companyregok'   => 'Your account has been created successfully. Soon you will receive an email with the necessary steps to complete your registration.',
    'companyemailok'   => 'Successful registration!. To complete the registration of your company in My Digital Card, we invite you to click on the following link.',
    'socialregok'   => 'Social network created successfully.',
    'branchregok'   => 'Company branch created successfully.',
    'branchrupdok'   => 'Company branch updated successfully.'


];
